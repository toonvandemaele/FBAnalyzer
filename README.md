# FB Analyzer
A command line tool to extract (& visualize) data from your personal facebook data. 

### 0. Download the data 
To request your personal data from facebook: if you go to 'settings' there is an option 'Your Facebook Information'. Here the data can be requested.

In the downloaded data folder, there is a folder with all messages stored as HTML files. By opening the index.html file in your browser, the wanted filename can be easily found. 

### 1. Extract 
The extract script extracts information (User, Day, Month, Year) from a certain messenger conversation
 and saves this list in a .csv file. One row corresponds to one message. 

usage: 
```bash
python3 extract.py -f <path/to/html/file> -s <path/to/save/csv>
```

### 2. Analyze

#### 2.1 Create plot 
A script is included to analyse a one-on-one conversation in time, it creates a plot of the amount of messages sent every month straight from the html file. 

```bash
python3 create_plot.py -f <path/to/html/file> 
```

#### 2.2 Notebooks
Two notebooks are included that show possible ways to analyze the generated csv data. For privacy reasons, I changed the names in the sample data. 

MessageContributions shows the contributions of different users in a group chat.
![Message contributions plot](results/sample_groupchat.png)

MessageCount shows the number of messages in a one on one chat over time. 
![Message contributions plot](results/sample_oneonone.png)
