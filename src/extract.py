#!/usr/bin/python3

import argparse
import sys

from modules.parser import MessageParser

if __name__ == '__main__':

    print('Starting fb data analytics program')

    parser = argparse.ArgumentParser(
        description="Fb data analysis",
        formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument('-f', '--file',
                help="HTML file location from which you want analytics data")

    parser.add_argument('-s', '--save',
                        help="save location (& filename) for the results (csv)")

    args = vars(parser.parse_args())

    if args['file'] is None:
        print('Please provide a proper filename')
        sys.exit(1)
    if args['save'] is None:
        print('Please provide a proper save location')
        sys.exit(1)

    message_analyzer = MessageParser(args['file'])
    message_analyzer.save_message_metadata(args['save'])

    print('message count:', message_analyzer.get_message_count())
