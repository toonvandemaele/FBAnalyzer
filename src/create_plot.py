#!/usr/bin/python3
from modules.parser import *

import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns
import pandas as pd

import argparse
import sys

if __name__ == '__main__':

    print('Starting fb plotting program')

    parser = argparse.ArgumentParser(
        description="Fb data analysis",
        formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument('-f', '--file',
                help="HTML file location from which you want analytics data")

    args = vars(parser.parse_args())

    if args['file'] is None:
        print('Please provide a proper filename')
        sys.exit(1)

    message_analyzer = MessageParser(args['file'])
    message_analyzer.save_message_metadata('temp.csv')

    # -------------------------------------------------------------------------#
    # Crappy code,  just for plotting
    # -------------------------------------------------------------------------#

    # Read and process data in one block to avoid not working blocks
    thread = pd.read_csv('temp.csv', sep=',', engine='python')
    # Convert months to numeric data
    thread['month'] = thread['month'].str.strip().map(
        {
        'January': 1,
        'February': 2,
        'March': 3,
        'April': 4,
        'May': 5,
        'June': 6,
        'July': 7,
        'August': 8,
        'September': 9,
        'October': 10,
        'November': 11,
        'December': 12
        }
    )
    thread['day'] = pd.to_numeric(thread['day'])
    thread['year'] = pd.to_numeric(thread['year'])
    # Group the data based on both month and year for chronological display
    sorted = thread[['year','month']].sort_values(['year', 'month'], ascending=[True, True])
    sorted_group = sorted.groupby(['year','month']).size().reset_index(name='counts')

    sorted_group['month'] = sorted_group['month'].map({
        1:'January',
        2:'February',
        3:'March',
        4:'April',
        5:'May',
        6:'June',
        7:'July',
        8:'August',
        9:'September',
        10:'October',
        11:'November',
        12:'December'})
    sorted_group["period"] = sorted_group["month"] + " " + sorted_group["year"].map(str)

    ax = sorted_group['counts'].plot.bar(figsize=(12,6), color='mediumvioletred', edgecolor='none')
    ax.set_xticklabels(
        sorted_group["period"]
    )
    sns.despine()
    ax.set_title("Evolution of messagecount per month", fontsize=18)
    plt.show()
