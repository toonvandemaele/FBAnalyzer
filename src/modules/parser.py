from bs4 import BeautifulSoup
import re
import numpy as np


class MessageParser:

    def __init__(self, filename):
        self.html = open(filename, 'r').read()

    def print_html(self):
        print(self.html)

    def get_message_count(self):
        parsed = BeautifulSoup(self.html, 'html.parser')
        messages = parsed.findAll('div', {'class': 'message'})
        return len(messages)

    def get_first_message(self):
        parsed = BeautifulSoup(self.html, 'html.parser')
        messages = parsed.findAll('div', {'class': 'message'})
        return messages[0], messages[len(messages)-1]

    @staticmethod
    def parse_message(message):
        user = re.compile('<span class=\"user\">(.*?)</span>', re.DOTALL).search(str(message)).group(1)

        # Convert date to numerical value
        date = re.compile('<span class=\"meta\">(.*?)</span>', re.DOTALL).search(str(message)).group(1)
        date = date.split(sep=',')
        day = date[1][1:]
        day = day.split(sep=' ')
        month = day[0]
        day = day[1]
        year = date[2][1:5]

        return user, day, month, year

    def save_message_metadata(self, file_location):
        parsed = BeautifulSoup(self.html, 'html.parser')
        messages = parsed.findAll('div', {'class': 'message'})

        users = []
        days = []
        months = []
        years = []
        for message in messages:
            user, day, month, year = self.parse_message(message)
            users.append(user)
            days.append(day)
            months.append(month)
            years.append(year)

        result = np.vstack([users, days, months, years])
        result = np.transpose(result)

        np.savetxt(file_location,
                   result,
                   delimiter=',',
                   header="user,day,month,year",
                   fmt="%s",
                   comments="")
